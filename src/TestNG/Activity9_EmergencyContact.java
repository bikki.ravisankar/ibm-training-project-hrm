package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity9_EmergencyContact {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void getEmergencyContacts() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
       
       //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String MyInfo2=driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).getText();
   	 System.out.println("Module Displayed is : " + MyInfo2);
    
   	 //Resize current window to the set dimension
      driver.manage().window().maximize();
      Thread.sleep(3000);
      
   	 //Click on MyInfo
   	driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).click();
   	Thread.sleep(3000);
   	//Click on Emergency Contacts  - #sidenav > li:nth-child(3) > a:nth-child(1)
   
       	driver.findElement(By.cssSelector("#sidenav > li:nth-child(3) > a:nth-child(1)")).click();
    	System.out.println("Emergency Contacts Option is Clicked");
     
     
    Thread.sleep(3000);
   //Get all the Emergency contacts information in table
    int TotalRows,TotalColumns;
    int i,j;
    String rowdata;
    
    TotalRows=driver.findElements(By.xpath("//table/tbody/tr")).size();
    TotalColumns=driver.findElements(By.xpath("//table/tbody/tr[1]/td")).size();
    System.out.println("Total number of rows="+TotalRows);
   
    
    for(i=1; i<=TotalRows; i++) {
    	//Get and Print Emergency contcats in each row
        WebElement row = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]"));
        rowdata=row.getText();
       System.out.println("Contact Info:"+ i +"= "+rowdata);
        
       // WebElement col = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[6]"));
        //columndata=col.getText();
      } //For Loop Close
        
   
    } //@Test close
  
   
    @AfterClass
    public void afterClass() {
        //Close browser
       driver.close();
    }
 
	
}
