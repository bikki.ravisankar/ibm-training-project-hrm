package TestNG;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity11_AddCandidateForRecruitment {
  
	WebDriver driver;
	Select dropdown;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void addCandidate() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
          
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Recruitment Menu appear - #menu_recruitment_viewRecruitmentModule > b:nth-child(1)
   	 String menuRecruit=driver.findElement(By.cssSelector("#menu_recruitment_viewRecruitmentModule > b:nth-child(1)")).getText();
   	 System.out.println("Module Selected is : " + menuRecruit);
   	 //Click on Recruitment menu
   	driver.findElement(By.cssSelector("#menu_recruitment_viewRecruitmentModule > b:nth-child(1)")).click();
   	Thread.sleep(2000);
   	
  //Resize current window to the set dimension
    driver.manage().window().maximize();
    Thread.sleep(3000);
   	
   	//click on Candidates link -#menu_recruitment_viewCandidates
    driver.findElement(By.cssSelector("#menu_recruitment_viewCandidates")).click();
    Thread.sleep(2000);
    //Click on Add button - #btnAdd
    driver.findElement(By.cssSelector("#btnAdd")).click();
    Thread.sleep(2000);
    
    //Add Candidate details for Recruitement
    String firstName,lastName,vacancyName;
    driver.findElement(By.id("addCandidate_firstName")).sendKeys("Manohar");
    driver.findElement(By.id("addCandidate_lastName")).sendKeys("B");
    driver.findElement(By.id("addCandidate_email")).sendKeys("manoharb@gmail.com");
    driver.findElement(By.id("addCandidate_contactNo")).sendKeys("9903765399");
   
    dropdown=new Select(driver.findElement(By.id("addCandidate_vacancy")));
	dropdown.selectByIndex(5);
	
	//Upload a Resume
	WebElement upload=driver.findElement(By.id("addCandidate_resume"));
	upload.sendKeys("C:\\Users\\RAVIBIKKI\\Documents\\Ravi\\Resume.docx");
	  
	Thread.sleep(3000);  
	
	//Click on Save button - #btnSave
	 driver.findElement(By.cssSelector("#btnSave")).click();
	 Thread.sleep(3000);
    
    
	  //click on Candidates link -#menu_recruitment_viewCandidates
	   driver.findElement(By.cssSelector("#menu_recruitment_viewCandidates")).click();
	   Thread.sleep(2000);
                
  //Get Column where New Vacancy exists
    int TotalRows;
    int TotalColumns;
    int rownum1,rownum2,i;
    String newVacancy,candidatefullName;
    
    String rowdata,columndata;
    TotalRows=driver.findElements(By.xpath("//table/tbody/tr")).size();
    TotalColumns=driver.findElements(By.xpath("//table/tbody/tr[1]/td")).size();
    System.out.println("Total number of rows="+TotalRows);
   
    
    for(i=1; i<=TotalRows-1; i++) {
    	//WebElement row1 = driver.findElement(By.linkText(vacancyName));
    	//Search for From Date in all rows
        WebElement row = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]"));
        
      //Print row data
        rowdata=row.getText();
        //System.out.println("row data "+i+"="+rowdata);
        
        WebElement col = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[4]"));
        columndata=col.getText();
       // System.out.println("column data "+i+"="+columndata);
        
        if(rowdata.contains("Automation"))
       	
          {
        	if(!columndata.contains("ravi b")) 
        	{
        		 continue;
        	}
       
           else
            {
        	
            rownum1=i;
            rownum2=i;
            System.out.println("Candidate Vacancy Found in Row= " + rownum2 );
            newVacancy=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
            System.out.println("Candidate Vacancy Name is :  " + newVacancy);
            WebElement col2 = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[3]"));
            candidatefullName=col2.getText();
       	    System.out.println("Candidate Full Name is :  " + candidatefullName);
            }
          }	//end if condition
        
         
       } //For Loop
  
    
    } //@Test
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       // driver.close();
    }
	
	
}
