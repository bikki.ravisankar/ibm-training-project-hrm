package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity7_AddQualification {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void addQualification() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
       
       //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String MyInfo2=driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).getText();
   	 System.out.println("Module Displayed is : " + MyInfo2);
    
   	 //Resize current window to the set dimension
      driver.manage().window().maximize();
      Thread.sleep(3000);
      
   	 //Click on MyInfo
   	driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).click();
   	Thread.sleep(3000);
   	//Click on Qualifications link  - #sidenav > li:nth-child(9) > a:nth-child(1)
    //driver.findElement(By.cssSelector(".selected > a:nth-child(1)")).click();
    if(driver.findElement(By.cssSelector("#sidenav > li:nth-child(9) > a:nth-child(1)")).isDisplayed())
      {
    	System.out.println("Qualifications Option is Visible");
    	driver.findElement(By.cssSelector("#sidenav > li:nth-child(9) > a:nth-child(1)")).click();
    	System.out.println("Qualifications Option is Clicked");
      }
    else
      {
    	System.out.println("Qualifications Option is NOT Visible");
      }
     
    Thread.sleep(3000);
   //Add Work Experience
    if(driver.findElement(By.cssSelector("#addWorkExperience")).isEnabled())
      {
    	driver.findElement(By.cssSelector("#addWorkExperience")).click(); //Add button - #addWorkExperience
    	Thread.sleep(5000);
        WebElement Company=driver.findElement(By.cssSelector("#experience_employer"));
        WebElement Jobtitle=driver.findElement(By.cssSelector("#experience_jobtitle"));
        WebElement From=driver.findElement(By.cssSelector("#experience_from_date"));
        WebElement To=driver.findElement(By.cssSelector("#experience_to_date"));
        
        Company.sendKeys("ABC");
        Jobtitle.sendKeys("Test Lead");
        From.clear();
        From.sendKeys("2010-01-01",Keys.RETURN);
        To.clear();
        To.sendKeys("2015-12-01",Keys.RETURN);
        Thread.sleep(3000);
        
        //Click Save button
        driver.findElement(By.cssSelector("#btnWorkExpSave")).click();
      }
    else
      {
    	System.out.println("Add Qualifications button is NOT Enabled");
      }
    
    
     
    }
  
   
    @AfterClass
    public void afterClass() {
        //Close browser
       //driver.close();
    }
 
	
}
