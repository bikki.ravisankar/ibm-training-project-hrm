package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity5_EditUser {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void editUser() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
       
       //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String MyInfo2=driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).getText();
   	 System.out.println("Module Clicked is : " + MyInfo2);
   	 
   	driver.findElement(By.cssSelector("#menu_pim_viewMyDetails > b:nth-child(1)")).click();
   	Thread.sleep(2000);
   	//Click on Edit button
    driver.findElement(By.cssSelector("#btnSave")).click();
    Thread.sleep(2000);
    //Edit Lastname
    WebElement Lastname=driver.findElement(By.cssSelector("#personal_txtEmpLastName"));
    Lastname.clear();
    Thread.sleep(3000);
    Lastname.sendKeys("Sharma",Keys.RETURN);
    Thread.sleep(3000);
    //Edit Gender
    WebElement Gender=driver.findElement(By.cssSelector("#personal_optGender_1"));
    if(Gender.getAttribute("checked")!="checked") 
    	Gender.click();
    else
        System.out.println("Gender Male already selected");
    
    
    System.out.println("Gender Male already selected");
    //Select or Edit Nationality
    WebElement chosen=driver.findElement(By.cssSelector("#personal_cmbNation"));
    
    Select dropdown=new Select(chosen);
    Thread.sleep(3000);
  //Select Option by visible text
    dropdown.selectByVisibleText("Indian");        
    Thread.sleep(3000);
    
    //Edit or Enter DOB
    WebElement DOB=driver.findElement(By.cssSelector("#personal_DOB"));
    DOB.clear();
    Thread.sleep(3000);
    DOB.sendKeys("1990-12-01",Keys.RETURN);
    Thread.sleep(3000);
    
    //Click Save button
    driver.findElement(By.cssSelector("#btnSave")).click();
  
    
    }
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       //driver.close();
    }
    
    
	
	
}
