package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity2_HeaderImage_URL {
  
WebDriver driver;
    
    @BeforeMethod
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
        
        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void openHrmWebSite() throws InterruptedException {
        // Get the url of the header image
    	String imgpath;
    	
    	WebElement imgurl = driver.findElement(By.cssSelector("#divLogo > img:nth-child(1)"));
    	
    	imgpath=imgurl.getAttribute("src");
            
        //Print the title of the page
        System.out.println("Get the url of the header image is : " + imgpath);
            
                                
        Thread.sleep(3000);
    }

    @AfterMethod
    public void afterMethod() {
    	
        driver.close();
    }
	
}
