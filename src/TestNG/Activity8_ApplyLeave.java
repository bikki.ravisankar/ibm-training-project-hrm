package TestNG;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity8_ApplyLeave {
  
	WebDriver driver;
	Select dropdown;
	Select dropdown2;
	Calendar calendar;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
       } //Before Class

    @Test
    public void applyLeave() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
       
       //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear - #menu_dashboard_index > b:nth-child(1)
   	 String Dashboard=driver.findElement(By.cssSelector("#menu_dashboard_index > b:nth-child(1)")).getText();
   	 System.out.println("Module Displayed is : " + Dashboard);
    
   	 //Resize current window to the set dimension
      driver.manage().window().maximize();
      Thread.sleep(3000);
      
   	 //Click on Dashboard
   	driver.findElement(By.cssSelector("#menu_dashboard_index > b:nth-child(1)")).click();
   	Thread.sleep(3000);
   	
  //Click on Apply Leave
   	driver.findElement(By.cssSelector(".quickLaungeContainer > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(4) > div:nth-child(1) > a:nth-child(1) > img:nth-child(1)")).click();
   	Thread.sleep(3000);
   	
   	//Leave > Apply page should be displayed
    if(driver.findElement(By.cssSelector("#menu_leave_applyLeave")).isDisplayed())
      {
    	System.out.println("Leave > Apply Page is Displayed");
    	dropdown=new Select(driver.findElement(By.id("applyleave_txtLeaveType")));
    	dropdown.selectByVisibleText("Paid Leave");
    	
    	// Create object of SimpleDateFormat class and decide the format
    	 DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
    	 
    	 //get current date time with Date()
    	 Date date = new Date();
    	 
    	 // Now format the date
    	 String date1= dateFormat.format(date);
        	
    	
    	WebElement From=driver.findElement(By.cssSelector("#applyleave_txtFromDate"));
    	From.clear();
    	String dtFrom="2020-02-06";
        //From.sendKeys(date1,Keys.RETURN);
    	From.sendKeys(dtFrom,Keys.RETURN);
    	
        String dtTo="2020-02-06";
        WebElement To=driver.findElement(By.cssSelector("#applyleave_txtToDate"));
        To.clear();
        //To.sendKeys(date1,Keys.RETURN);
        To.sendKeys(dtTo,Keys.RETURN);
        
        dropdown2=new Select(driver.findElement(By.id("applyleave_duration_duration")));
    	dropdown2.selectByVisibleText("Full Day");
        //Click on Apply button
        driver.findElement(By.cssSelector("#applyBtn")).click();
    	
      }
    else
      {
    	System.out.println("Leave > Apply Page is NOT Displayed (or) Cannot apply leave on a Holiday");
      }
     
    Thread.sleep(3000);
    //My Leave status
    driver.findElement(By.cssSelector("#menu_leave_viewMyLeaveList")).click();
    Thread.sleep(3000);
    //Get Column where From date exeists
    int TotalRows;
    int TotalColumns;
    int rownum1,rownum2,i;
    String LeaveDates;
    
    String rowdata,columndata;
    TotalRows=driver.findElements(By.xpath("//table/tbody/tr")).size();
    TotalColumns=driver.findElements(By.xpath("//table/tbody/tr[1]/td")).size();
    System.out.println("Total number of rows="+TotalRows);
   
    
    for(i=1; i<=TotalRows-1; i++) {
    	//Search for From Date in all rows
        WebElement row = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]"));
        rowdata=row.getText();
       // System.out.println("row data="+rowdata);
        
        WebElement col = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[6]"));
        columndata=col.getText();
       // System.out.println("column data="+columndata);
        
        if(rowdata.contains("2020-02-06"))
       	
          {
        	if(!columndata.contains("Pending")) 
        	{
        		 continue;
        	}
       
           else
            {
        	
            rownum1=i;
            rownum2=i;
            System.out.println("Your Leave Request Found in Row= " + rownum2 );
            LeaveDates=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[1]")).getText();
            System.out.println("Your Applied Leaves on :  " + LeaveDates);
       	    System.out.println("Your Leave Request Status is :  " + col.getText() );
            }
          }	
        
         
       } //For Loop
    
   
    } //Test
  
   
    @AfterClass
    public void afterClass() {
        //Close browser
       //driver.close();
    }
 
	
}
