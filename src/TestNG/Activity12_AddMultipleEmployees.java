package TestNG;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity12_AddMultipleEmployees {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void AddMultipleEmployees() throws InterruptedException, IOException, CsvException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
          
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String pim2=driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).getText();
   	 System.out.println("PIM Module Text is : " + pim2);
   	 
   	//Click on PIM Mebu 
   	driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).click();
   	Thread.sleep(2000);
   	
    //Resize current window to the set dimension
    driver.manage().window().maximize();
    Thread.sleep(2000);
    
   	//Click on Employee List section
    driver.findElement(By.cssSelector("#menu_pim_viewEmployeeList")).click();
    Thread.sleep(2000);
    //Click on Add button to add a new Employee
    driver.findElement(By.cssSelector("#btnAdd")).click();
    Thread.sleep(2000);
    //Get the data from CSV file
  //Load CSV file
    CSVReader reader = new CSVReader(new FileReader("C:\\Users\\RAVIBIKKI\\eclipse-workspace\\Batch-2-HRM\\src\\TestNG\\activity12-MultipleEmployees.csv"));

    //Load content into list
    List<String[]> list = reader.readAll();
    System.out.println("Total number of rows are: " + list.size());

    //Create Iterator reference
    Iterator<String[]> itr = list.iterator();

    //Print CSV file data
    //while(itr.hasNext()) {
       // String[] str = itr.next();

       // System.out.print("Values are: ");
      //  for(int i=0;i<str.length;i++) {
        //    System.out.print(" " + str[i]);
      //  }
      //  System.out.print(System.getProperty("line.separator"));
  //  }
    
    //Iterate all values
    while(itr.hasNext()) {
    	String[] str = itr.next();
        for(int i=0;i<str.length-1;i++) {
        	
        	
            //System.out.print("Values are: ");
        	
        		//System.out.print("" + str[i]+""+str[i+1]);
                driver.findElement(By.id("firstName")).sendKeys(str[i+1]); //1
                driver.findElement(By.id("lastName")).sendKeys(str[i]); //0
                driver.findElement(By.id("chkLogin")).click();  //Select Create Login Details check box
                
                Thread.sleep(3000);
                if(driver.findElement(By.id("chkLogin")).isSelected()==true)
                {
                	 String fullName=str[i]+str[i+1]; 
                	 driver.findElement(By.id("user_name")).sendKeys(fullName);
                     driver.findElement(By.id("user_password")).sendKeys("pa$$w0rd");
                     driver.findElement(By.id("re_password")).sendKeys("pa$$w0rd");
                     driver.findElement(By.id("btnSave")).click();
                     Thread.sleep(2000);
                   //Click on Add Employee option
                    // driver.findElement(By.id("menu_pim_addEmployee")).click();
                     //continue;
                   //Click on Employee List section
                     driver.findElement(By.cssSelector("#menu_pim_viewEmployeeList")).click();
                     Thread.sleep(2000);
                     //Click on Add button to add a new Employee
                     driver.findElement(By.cssSelector("#btnAdd")).click();
                     Thread.sleep(2000); 
                }
                else
                {
                	System.out.println("Cannot add a New Employee as already Exists");
                }	
                	    
                
           
        } //end of for
        System.out.println(" ");
       
    } //end of while
    
    reader.close();
    
    
        
    
    Thread.sleep(2000);
    //Click on Admin Module
    driver.findElement(By.cssSelector("#menu_admin_viewAdminModule > b:nth-child(1)")).click();
  //Verify creation of Employee
    //WebElement emp = driver.findElement(By.cssSelector("tr.even:nth-child(2) > td:nth-child(2) > a:nth-child(1)"));
    
        	//String fullName2=str[i]+str[i+1];	
        	//String emp2=driver.findElement(By.linkText(fullName2)).getText();
        	//System.out.println("Employee Username is  : " + emp2);
            //Assert.assertEquals("UniversityAlfred", fullName2);
            //Print the title of the page
            //System.out.println("Employee : " + emp2 + " has successfully created");
    
  //Get Column where New Vacancy exists
    int TotalRows;
    int TotalColumns;
    int rownum1,rownum2,i;
    String newEmployee,JobTitle;
    
    String rowdata,columndata;
    TotalRows=driver.findElements(By.xpath("//table/tbody/tr")).size();
    TotalColumns=driver.findElements(By.xpath("//table/tbody/tr[1]/td")).size();
    System.out.println("Total number of rows="+TotalRows);
   
    
    for(i=1; i<=TotalRows-1; i++) {
    	//WebElement row1 = driver.findElement(By.linkText(vacancyName));
    	//Search for From Date in all rows
        WebElement row = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]"));
        
      //Print row data
        rowdata=row.getText();
        //System.out.println("row data "+i+"="+rowdata);
        
        WebElement col = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]"));
        columndata=col.getText();
       // System.out.println("column data "+i+"="+columndata);
        
        if(!rowdata.contains("Alfred"))
       	
          {
        	  if(!rowdata.contains("Gerty")) 
        	  {
        		  if(!rowdata.contains("Android")) 
        		  {
        			  if(!rowdata.contains("Rubble"))
            	      {
            		    continue;
            	       } 
        			  else
        			  {
        				   rownum1=i;
        	        	   rownum2=i;
        	        	   System.out.println("Your New Employee Found in Row= " + rownum2 );
        	        	   newEmployee=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
        	        	   System.out.println("Your Empolyee Full Name is :  " + newEmployee);
        			  }
        		  }
        		  else
        		  {
        		   rownum1=i;
               	   rownum2=i;
               	   System.out.println("Your New Employee Found in Row= " + rownum2 );
               	   newEmployee=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
               	   System.out.println("Your Empolyee Full Name is :  " + newEmployee);
        		  }
        			  
        	 }	
        	  else
        	  {
        		  
        	        	
        	   rownum1=i;
        	   rownum2=i;
        	   System.out.println("Your New Employee Found in Row= " + rownum2 );
        	   newEmployee=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
        	   System.out.println("Your Empolyee Full Name is :  " + newEmployee);
        	           
        	             
        	  }
          
          
          }	//end if condition
       
           else
            {
        	
            rownum1=i;
            rownum2=i;
            System.out.println("Your New Employee Found in Row= " + rownum2 );
            newEmployee=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
            System.out.println("Your Empolyee Full Name is :  " + newEmployee);
           
            } //end of mail else
          
        
         
       } //For Loop
       
   
    
    
   } //@Test
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       // driver.close();
    }
	
	
}
