package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Activity14_CreateMultipleVacancies {
  
	WebDriver driver;
	Select dropdown;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void createMultipleVacancies() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
          
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Recruitment Menu appear - #menu_recruitment_viewRecruitmentModule > b:nth-child(1)
   	 String menuRecruit=driver.findElement(By.cssSelector("#menu_recruitment_viewRecruitmentModule > b:nth-child(1)")).getText();
   	 System.out.println("Module Selected is : " + menuRecruit);
   	 //Click on Recruitment menu
   	driver.findElement(By.cssSelector("#menu_recruitment_viewRecruitmentModule > b:nth-child(1)")).click();
   	Thread.sleep(2000);
   	
  //Resize current window to the set dimension
    driver.manage().window().maximize();
    Thread.sleep(3000);
   	
   	//click on Vacancies link - #menu_recruitment_viewJobVacancy
    driver.findElement(By.cssSelector("#menu_recruitment_viewJobVacancy")).click();
    Thread.sleep(2000);
    //Click on Add button - #btnAdd
    driver.findElement(By.cssSelector("#btnAdd")).click();
    Thread.sleep(2000);
    // adding vacancy start here -Read excel
    try {
         String filePath="C:\\Users\\RAVIBIKKI\\eclipse-workspace\\multipleVacancies.xlsx";
        FileInputStream file = new FileInputStream(filePath);
        //Create Workbook instance holding reference to Excel file
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        //Get first sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row1 = sheet.getRow(0);
        XSSFRow row2 = sheet.getRow(1);
        int colNum = row1.getLastCellNum();
        System.out.println("Total Number of Columns in the excel is : "+colNum);
        int rowNum = sheet.getLastRowNum()+1;
        System.out.println("Total Number of Rows in the excel is : "+rowNum);
        int TotalCells=colNum * rowNum;
        System.out.println("Total Number of Cells in the excel is : "+TotalCells);
        //Iterate through each rows one by one
        
        Iterator<Row> rowIterator = sheet.iterator();

        while (rowIterator.hasNext()) {
        	
        	
            Row row = rowIterator.next();
            
          
            //For each row, iterate through all the columns

            Iterator<Cell> cellIterator = row.cellIterator();

            while (cellIterator.hasNext()) {

                Cell cell = cellIterator.next();

                //Check the cell type and format accordingly
               for(int i=1;i<=TotalCells;i++) 
               {
            	   //Add Job Title
            	   dropdown=new Select(driver.findElement(By.id("addJobVacancy_jobTitle"))); 
                   dropdown.selectByVisibleText(cell.toString()); 
                  //Add Job Name
                   cell=cellIterator.next();
                   driver.findElement(By.id("addJobVacancy_name")).sendKeys(cell.toString());
                   System.out.println("New Vacancy Name is: "+cell.toString());
                   //Add Mgr
                   driver.findElement(By.id("addJobVacancy_hiringManager")).click();
                   cell=cellIterator.next();
                   driver.findElement(By.id("addJobVacancy_hiringManager")).sendKeys(cell.toString());
                   //Add Number of positions
                   cell=cellIterator.next();
            	   driver.findElement(By.id("addJobVacancy_noOfPositions")).sendKeys(cell.toString());
            	   //Add Job Description
            	   cell=cellIterator.next();
            	   driver.findElement(By.id("addJobVacancy_description")).sendKeys(cell.toString()); 
            	   
            	    String hiringMgr,vacancyName;
            	    
            	    vacancyName=driver.findElement(By.id("addJobVacancy_name")).getAttribute("value");
            	  
            	    hiringMgr=driver.findElement(By.id("addJobVacancy_hiringManager")).getAttribute("value");
            	   
            	  //Click on Save button - #btnSave
            	    driver.findElement(By.cssSelector("#btnSave")).click();
            	    Thread.sleep(3000);
            	    
            	    //End of adding vacancy
            	  //click on Vacancies link - #menu_recruitment_viewJobVacancy
            	    driver.findElement(By.cssSelector("#menu_recruitment_viewJobVacancy")).click();
            	    Thread.sleep(2000);
            	  //Click on Add button - #btnAdd
            	    driver.findElement(By.cssSelector("#btnAdd")).click();
            	    Thread.sleep(2000);
             	   
               } //For Loop
               
             
            }

            System.out.println("");
          
        }

        file.close();

        workbook.close();

    }

    catch (Exception e) {

        e.printStackTrace();

    }
    
    //Excel close
    
  //click on Vacancies link - #menu_recruitment_viewJobVacancy
    driver.findElement(By.cssSelector("#menu_recruitment_viewJobVacancy")).click();
    Thread.sleep(2000);  
    
                
  //Get Column where New Vacancy exists
    int TotalRows;
    int TotalColumns;
    int rownum1,rownum2,i;
    String newVacancy,JobTitle;
    
    String rowdata,columndata;
    TotalRows=driver.findElements(By.xpath("//table/tbody/tr")).size();
    TotalColumns=driver.findElements(By.xpath("//table/tbody/tr[1]/td")).size();
    System.out.println("Total number of rows="+TotalRows);
   
    
    for(i=1; i<=TotalRows-1; i++) {
    	//WebElement row1 = driver.findElement(By.linkText(vacancyName));
    	//Search for From Date in all rows
        WebElement row = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]"));
        
      //Print row data
        rowdata=row.getText();
        //System.out.println("row data "+i+"="+rowdata);
        
        WebElement col = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[4]"));
        columndata=col.getText();
       // System.out.println("column data "+i+"="+columndata);
        
        if(rowdata.contains("Mobile"))
       	
          {
        	
             if(rowdata.contains("DevOps"))
              {
        	   if(!columndata.contains("ravi b")) 
           	    {
           		 continue;
           	    }
          
               else
                {
           	
                 rownum1=i;
                 rownum2=i;
                 System.out.println("Your New Vacancy Found in Row= " + rownum2 );
                 newVacancy=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
                 System.out.println("Your New Vacancy is :  " + newVacancy);
                 WebElement col2 = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[3]"));
                 JobTitle=col2.getText();
          	     System.out.println("Your Job Title is :  " + JobTitle);
                }
              } //embed if
        	if(!columndata.contains("ravi b")) 
        	{
        		 continue;
        	}
       
           else
            {
        	
            rownum1=i;
            rownum2=i;
            System.out.println("Your New Vacancy Found in Row= " + rownum2 );
            newVacancy=driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]")).getText();
            System.out.println("Your New Vacancy is :  " + newVacancy);
            WebElement col2 = driver.findElement(By.xpath("//table/tbody/tr[" + i + "]/td[3]"));
            JobTitle=col2.getText();
       	    System.out.println("Your Job Title is :  " + JobTitle);
            }
          }	//end if condition
       
         
       } //For Loop
  
    
    } //@Test
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       // driver.close();
    }
	
	
}
