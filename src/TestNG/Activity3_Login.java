package TestNG;


import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;




public class Activity3_Login {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginTest() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
        
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
        
                                
        Thread.sleep(5000);
    }

    @AfterClass
    public void afterClass() {
        //Close browser
        driver.close();
    }
	
	
}
