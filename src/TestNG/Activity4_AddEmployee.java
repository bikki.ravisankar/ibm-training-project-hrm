package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity4_AddEmployee {
  
	WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void loginAndAddEmployee() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
          
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String pim2=driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).getText();
   	 System.out.println("PIM Module Text is : " + pim2);
   	 
   	driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).click();
   	Thread.sleep(2000);
    driver.findElement(By.cssSelector("#menu_pim_viewEmployeeList")).click();
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("#btnAdd")).click();
    Thread.sleep(2000);
    driver.findElement(By.id("firstName")).sendKeys("Ravi");
    driver.findElement(By.id("lastName")).sendKeys("New");
    driver.findElement(By.id("chkLogin")).click();
    Thread.sleep(3000);
            
    if(driver.findElement(By.id("chkLogin")).isSelected()==true)
    {
    	 driver.findElement(By.id("user_name")).sendKeys("ravinew");
         driver.findElement(By.id("user_password")).sendKeys("pa$$w0rd");
         driver.findElement(By.id("re_password")).sendKeys("pa$$w0rd");
         driver.findElement(By.id("btnSave")).click();
    }
    else
    {
    	System.out.println("Cannot add a New Employee");
    }
    Thread.sleep(3000);
    driver.findElement(By.cssSelector("#menu_admin_viewAdminModule > b:nth-child(1)")).click();
  //Verify creation of Employee
    //WebElement emp = driver.findElement(By.cssSelector("tr.even:nth-child(2) > td:nth-child(2) > a:nth-child(1)"));
    String emp2=driver.findElement(By.linkText("ravinew")).getText();
    System.out.println("Employee Username is  : " + emp2);
    Assert.assertEquals("ravinew", emp2);
    //Print the title of the page
    System.out.println("Employee : " + emp2 + " has successfully created");
    
    }
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       // driver.close();
    }
	
	
}
