package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity13_UploadCSVFile {
  
	WebDriver driver;
	Actions actions;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void uploadCSVFile() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
        //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
          
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 String pim2=driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).getText();
   	 System.out.println("PIM Module Text is : " + pim2);
   	 //Click on PIM Menu
   	driver.findElement(By.cssSelector("#menu_pim_viewPimModule > b:nth-child(1)")).click();
   	Thread.sleep(2000);
  //Resize current window to the set dimension
    driver.manage().window().maximize();
    Thread.sleep(2000);
    //Mouse Over a link using Actions
  //Navigate to “Configuration -> Data Import”
    actions = new Actions(driver);
    WebElement Configuration=driver.findElement(By.xpath("//a[contains(@id,'menu_pim_Configuration')]"));
    actions.moveToElement(Configuration).perform();
   	  
  
    driver.findElement(By.xpath("//a[contains(@id,'menu_pim_Configuration')]")).click();
       //driver.findElement(By.cssSelector("#menu_pim_Configuration")).click();
    Thread.sleep(2000);
    
    driver.findElement(By.cssSelector("#menu_admin_pimCsvImport")).click();
    
    Thread.sleep(2000);
    if(driver.findElement(By.cssSelector("#pimCsvImport_csvFile")).isDisplayed())
    {
    	driver.findElement(By.cssSelector("#pimCsvImport_csvFile")).sendKeys("C:\\Users\\RAVIBIKKI\\eclipse-workspace\\activity14-1.csv");
    	Thread.sleep(3000);
    	driver.findElement(By.cssSelector("#btnSave")).click();
    	Thread.sleep(3000);
    }
    else
    {
    	System.out.println("Cannot upload a file");
    }
    
        
    } //end of @Test
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       driver.close();
    }
	
	
}
