package TestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity6_DirectoryMenu {
  
	WebDriver driver;
	WebDriverWait wait;
    Assert Ass;
    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();

        //Open browser
        driver.get("http://alchemy.hguy.co/orangehrm");
    }

    @Test
    public void menuDirectory() throws InterruptedException {
        //Find the username and password fields
        WebElement username = driver.findElement(By.id("txtUsername"));
        WebElement password = driver.findElement(By.id("txtPassword"));
        
        //Enter credentials
        username.sendKeys("orange");
        password.sendKeys("orangepassword123");
        
        //Click login
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        
        //Read login message
        String loginMessage = driver.findElement(By.id("welcome")).getText();
       
       //Verify Welcome is displayed in login/home page 
        Assert.assertTrue(loginMessage.contains("Welcome"));
      
        //Print the title of the page
        System.out.println("Print Login User details : " + loginMessage);
                                       
        Thread.sleep(2000);
      //Wait for Menu appear
   	 WebElement menuDirectory=driver.findElement(By.cssSelector("#menu_directory_viewDirectory > b:nth-child(1)"));
   	 
   	 if(menuDirectory.isDisplayed())
   	    {
   		System.out.println("Direcory Menu is Visible"); 
   	    }
   	 else {
   		System.out.println("Direcory Menu is NOT Visible"); 
   	      }
   	 
   	 System.out.println("Menu Displayed is : " + menuDirectory.getText());
   	 
   	 //Wait for Webelement and check if Clickable or not
   	wait = new WebDriverWait(driver, 10);
   	wait.until(ExpectedConditions.elementToBeClickable(menuDirectory));
   	System.out.println("Direcory Menu is Clickable"); 
   	 
   	 //Click Directory Menu
   	menuDirectory.click();
   	Thread.sleep(2000);
   	
  //Verify Directory page heading as Search Directory
    WebElement DirectoryHeading=driver.findElement(By.cssSelector(".toggle"));
    String DirectHead=DirectoryHeading.getText();
   if(DirectHead=="Search Directory")
      {
	   System.out.println("Direcory Page Heading is Search Directory");   
      }
   else
      {
	   System.out.println("Direcory Page Heading is NOT Search Directory");   
      }   
    Thread.sleep(3000);
         
  }   //End of @Test
  
     

    @AfterClass
    public void afterClass() {
        //Close browser
       //driver.close();
    }
    
    
	
	
}
